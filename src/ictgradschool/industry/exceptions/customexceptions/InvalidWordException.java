package ictgradschool.industry.exceptions.customexceptions;

public class InvalidWordException extends Exception {

    InvalidWordException(){

    }

    InvalidWordException(String msg){
        super(msg);
    }

    public InvalidWordException(Throwable cause) {
        super(cause);
    }

    public InvalidWordException(String message, Throwable cause) {
        super(message, cause);
    }
}
