package ictgradschool.industry.exceptions.customexceptions;

import ictgradschool.Keyboard;
import org.omg.PortableInterceptor.ServerRequestInfo;

public class InputGame {

    InputGame(){}

    public String getUserInput(){

        System.out.print("Enter your things: ");

        String userInputThings = Keyboard.readInput();

        return userInputThings;
    }

    public void start(){

        String result = getUserInput();

        String firstLetter = result.substring(0,1);

        for(int i = 0; i < result.length(); i++){

            if(result.indexOf(" ") == i){

                String secondLetter = result.charAt(i+1)+"";

                System.out.println("You entered: " + firstLetter +" "+ secondLetter);

            }else{

                System.out.println("You entered: " + firstLetter);

                break;
            }
        }
    }

    public static void main(String[] args) {
        InputGame i = new InputGame();
        i.start();
    }
}
