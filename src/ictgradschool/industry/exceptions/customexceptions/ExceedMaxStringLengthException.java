package ictgradschool.industry.exceptions.customexceptions;

public class ExceedMaxStringLengthException extends Exception {

    public ExceedMaxStringLengthException(String message) {
        super(message);
    }

    public ExceedMaxStringLengthException(Throwable cause) {
        super(cause);
    }

    public ExceedMaxStringLengthException(String message, Throwable cause) {
        super(message, cause);
    }
}
